News
====

1.0.5
-----

*Release date: 20130604*

* Create converter object.

1.0.4
-----

*Release date: 20130520*

* Parse microseconds.

1.0.3.2
-------

*Release date: 20130218*

* Allow origin to be expressed only as HH.

1.0.3.1
-------

*Release date: 20130214*

* Fix minor typo in code.

1.0.3
-----

*Release date: 20130214*

* Create parser/formatter objects to avoid repeated parsing.
* Happy Valentine's!

1.0.2
-----

*Release date: 20120823*

* Fixed regexp that parses the origin.

1.0.1
-----

*Release date: 20120818*

* Parse RFC3339 dates.

1.0
---

*Release date: 20120529*

* Refactored code so that it works with more dates.
* Using modern-package-template for directory structure.
